'use strict';
function showAlert(stringAlert){
  if(stringAlert == 'success'){
    $('#alertSuccess').show('slow');
          setTimeout(function() {
            $('#alertSuccess').hide('slow');
          }, 3000);
  }
  if(stringAlert =='info'){
    $('#alertInfo').show("slow");
        setTimeout(function() {
          $('#alertInfo').hide('slow');
          }, 3000);
  }
  if(stringAlert =='alert'){
    $('#alert').show("slow");
        setTimeout(function() {
          $('#alertInfo').hide('slow');
          }, 3000);
  }
}

angular.module('equivalenciaAppApp', [
  'ngCookies',
  'ngResource',
  'ngRoute'
])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/list', {
        templateUrl: 'views/list.html',
        controller: 'ListCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

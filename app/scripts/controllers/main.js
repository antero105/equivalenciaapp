'use strict';
 var courseSelected = [];
 var optionSelected;
 var hc;
 var code;
 

angular.module('equivalenciaAppApp')
  .controller('MainCtrl', function ($scope,$http) {
    //$scope.courses = activeCourses;
    $http.get('../serverSide/index.php/getActiveCourses').success(function(data){
        $scope.courses = data.activeCourses;
        console.log(data);  
    }).error(function(data){
        console.log(data);
    });
    $('#id').keyup(function () { 
      this.value = this.value.replace(/[^0-9\.]/g,'');
    });
    
      
    $scope.setOption = function(option){
      var optionArray = option.split('&');
      optionSelected = optionArray[0];
      hc = optionArray[1];
      code = optionArray[2];


      }
    $scope.add = function(studentId,studentScore){
      if(typeof studentId == 'undefined' || typeof optionSelected == 'undefined' || typeof studentScore == 'undefined' || parseInt(studentScore) < 10  || parseInt(studentScore) > 20){
          showAlert('info');
        }
      else{
        $('#table tr td').each(function(){
             
        });
        if(isNaN(studentScore) && studentScore.toUpperCase() == 'AP'){
          courseSelected.push({
            studentId : parseInt(studentId),
            score : studentScore.toUpperCase(),
            courseName : optionSelected,
            hc : parseInt(hc),
            code : code
          });
          $('#id').prop('disabled', true);
          $('#options').focus();
        }
        else if(!isNaN(studentScore)){
          courseSelected.push({
            studentId : parseInt(studentId),
            score : parseInt(studentScore),
            courseName : optionSelected,
            hc : parseInt(hc),
            code : code
          });
          showAlert('success');
          
          $('#id').prop('disabled', true);
          $('#options').focus();
        }
        

      }
      $scope.delete = function(code){
        var courseSelectedLength= Object.keys(courseSelected).length;
          for(var i=0;i<courseSelectedLength;i++){
            if(courseSelected[i].code == code){
              courseSelected.splice(i,1);
                }
            }
        }
        
        $scope.save = function(){
        
        }
        $scope.courseSelected = courseSelected;
  }
});
